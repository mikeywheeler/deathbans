package me.goximo.net;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

public class DbListeners implements Listener {

	Deathbans plugin;

	public DbListeners(Deathbans instance) {
		plugin = instance;
	}

	@EventHandler
	public void onDeath(PlayerDeathEvent event) {
		Player player = event.getEntity();
		String deathmsg = event.getDeathMessage();
		setDead(player, deathmsg);
	}
	
	@EventHandler
	public void onLogin(PlayerLoginEvent event) {
		Player player = event.getPlayer();
		if (isDead(player) == true) {
			event.disallow(Result.KICK_OTHER, (ChatColor.YELLOW + "You are currently dead for another " + ChatColor.RED + getTime(player)));
		} else {
			setAlive(player);
		}
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		newPlayerFile(event.getPlayer());
	}
	
	public void setDead(Player player, String deathmsg) {
		File pdir = new File("plugins/Deathbans/players");
		File playerFile = new File(pdir, player.getName() + ".yml");
		File cfgFile = new File("plugins/Deathbans", "config.yml");
		YamlConfiguration plCfg = YamlConfiguration.loadConfiguration(cfgFile);
		YamlConfiguration playerYaml = YamlConfiguration.loadConfiguration(playerFile);
		
		long ct = System.currentTimeMillis();
		long min = 60000;
		int length = plCfg.getInt("Minutes");
		
		Bukkit.broadcastMessage("Minutes");
		long unbantime = ct + (min*length);
		playerYaml.set("Information.Dead", true);
		playerYaml.set("Information.Time", unbantime);
		try {
			playerYaml.save(playerFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	    player.kickPlayer(ChatColor.RED + deathmsg + ChatColor.YELLOW + "\nYou will be able to return in " + getTime(player));
	}
	
	public void setAlive(Player player) {
		File pdir = new File("plugins/Deathbans/players");
		File playerFile = new File(pdir, player.getName() + ".yml");
		YamlConfiguration playerYaml = YamlConfiguration.loadConfiguration(playerFile);
		
		playerYaml.set("Information.Dead", false);
		playerYaml.set("Information.Time", 0);
		try {
			playerYaml.save(playerFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public boolean isDead(Player player) {
		File pdir = new File("plugins/Deathbans/players");
		File playerFile = new File(pdir, player.getName() + ".yml");
		YamlConfiguration playerYaml = YamlConfiguration.loadConfiguration(playerFile);
		
		long ct = System.currentTimeMillis();
		long ut = playerYaml.getLong("Information.Time");
		
		if(ct < ut) {
			return true;
		} else {
			return false;
		}
	}
	
	public String getTime(Player player) {
		File pdir = new File("plugins/Deathbans/players");
		File playerFile = new File(pdir, player.getName() + ".yml");
		YamlConfiguration playerYaml = YamlConfiguration.loadConfiguration(playerFile);
		
		long ct = System.currentTimeMillis();
		long ut = playerYaml.getLong("Information.Time");
		
		int minutesleft = (int) (((ut-ct)/60000)+1);
		
		if(minutesleft > 60) {
			int hours = minutesleft / 60;
			int minutes = minutesleft % 60;
			String tleft = new String(hours + " Hours and " + minutes + " Minutes");
			return tleft;
		} else {
			String tleft = new String(minutesleft + " Minutes");
			return tleft;
		}
		
	
	}
	
	public void newPlayerFile(Player player) {
		File pdir = new File("plugins/Deathbans/players");
		File playerFile = new File(pdir, player.getName() + ".yml");
		
		if(!playerFile.exists()) {
			try {
				playerFile.createNewFile();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		
		YamlConfiguration playerYaml = YamlConfiguration.loadConfiguration(playerFile);
		
		final Map<String, Object> defParams = new HashMap<String, Object>();
		playerYaml.options().copyDefaults(true);   

		defParams.put("Information.Dead", false);
		defParams.put("Information.Time", "0");

		for (final Entry <String, Object> e : defParams.entrySet())
			if (!playerYaml.contains(e.getKey()))
				playerYaml.set(e.getKey(), e.getValue());
		 
			try {
				playerYaml.save(playerFile);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
	}
}

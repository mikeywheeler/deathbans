package me.goximo.net;

import java.io.File;
import java.io.IOException;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Deathbans extends JavaPlugin {

	public void onEnable() {
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(new DbListeners(this), this);
		saveDefaultConfig();
		File pdir = new File(getDataFolder() + "\\players");
		pdir.mkdirs();
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("revive")) {
			if(args.length == 1) {
				String playerName = args[0].toString();
				if (sender instanceof Player) {
					Player player = (Player) sender;
					if(player.hasPermission("command.revive")) {
						revive(playerName, player);
					}
				} else {
					revive(playerName, sender);
				}
			}
		}
		
		return true;
	}
	
	public void revive(String playerName, CommandSender sender) {
		File playerFile = new File(getDataFolder() + "\\players\\" + playerName + ".yml");
		YamlConfiguration playerYaml = YamlConfiguration.loadConfiguration(playerFile);
		if(playerFile.exists()) {
			playerYaml.set("Information.Dead", false);
			playerYaml.set("Information.Time", 0);
			try {
				playerYaml.save(playerFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
			sender.sendMessage(ChatColor.GRAY + "You've successfully revived: " + ChatColor.YELLOW + playerName);
		} else {
			sender.sendMessage(ChatColor.RED + "The player " + playerName + " does not exist");
		}
	}
	
}
